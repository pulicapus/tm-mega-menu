( function( $ ) {
	'use strict';

	/*
	 * debouncedresize: special jQuery event that happens once after a window resize
	 *
	 * latest version and complete README available on Github:
	 * https://github.com/louisremi/jquery-smartresize
	 *
	 * Copyright 2012 @louis_remi
	 * Licensed under the MIT license.
	 */
	var $event = $.event,
		$special,
		resizeTimeout;
	$special = $event.special.debouncedresize = {
		setup: function() {
			$( this ).on( 'resize', $special.handler );
		},
		teardown: function() {
			$( this ).off( 'resize', $special.handler );
		},
		handler: function( event, execAsap ) {
			// Save the context
			var context = this,
				args = arguments,
				dispatch = function() {
					// set correct event type
					event.type = 'debouncedresize';
					$event.dispatch.apply( context, args );
				};
			if ( resizeTimeout ) {
				clearTimeout( resizeTimeout );
			}
			if ( execAsap ) {
				dispatch();
			} else {
				resizeTimeout = setTimeout( dispatch, $special.threshold );
			}
		},
		threshold: 150
	};

	/**
	 * Mega Menu jQuery Plugin
	 */
	$.fn.megaMenu = function ( options ) {

		var menu = $( this ),
			durationTimeout,
			triggerFullscreen  = 1200,
			triggerDesktop     = 970,
			triggerTablet      = menu.data( 'mobile-trigger' ),
			subClass           = '.tm-mega-menu-sub',
			mobileOnClass      = 'mega-menu-mobile-on',
			mobileTriggerClass = '.tm-mega-menu-mobile-trigger',
			parentMegaClass    = '.tm-mega-menu-has-children',
			inTransitionClass  = 'tm-in-transition',
			forceHideClass     = 'tm-force-hide',
			hoverClass         = 'tm-mega-menu-hover',
			clickGoClass       = 'mega-click-click-go',
			noJsClass          = 'tm-mega-no-js',
			megaToggleOnClass  = 'mega-toggle-on',
			hideMobileClass    = 'item-hide-mobile',
			isTouchDevice      = 'ontouchstart' in window || 0 < navigator.maxTouchPoints || 0 < navigator.msMaxTouchPoints || window.innerWidth <= triggerTablet,
			isMobile;

		menu.settings = $.extend( {
			event:         menu.data( 'event' ),
			effect:        menu.data( 'effect' ),
			parent:        menu.data( 'parent-selector' ),
			direction:     menu.data( 'direction' ),
			duration:      menu.data( 'duration' ),
			mobileButton: menu.data( 'mobile-button' )
		}, options );

		var isInMegamenu = function( el ) {
			return !!el.parents( 'li.item-type-megamenu' ).length;
		};

		var switchMobile = function() {
			if ( window.innerWidth <= triggerTablet ) {
				mobileOn();
			} else {
				mobileOff();
			}
		};

		var mobileOn = function() {

			detachStyles();

			if( isMobile ) {
				return;
			}

			menu
				.addClass( mobileOnClass )
				.siblings( mobileTriggerClass )
				.addClass( mobileOnClass );

			isMobile = true;
		};

		var mobileOff = function() {

			appendStyles();

			if( !isMobile && undefined !== isMobile ) {
				return;
			}

			menu
				.removeClass( mobileOnClass )
				.siblings( mobileTriggerClass )
				.removeClass( mobileOnClass );

			isMobile = false;
		};

		var getMenuWidth = function ( string_width ) {
			var width = 0;
			if ( 0 <= string_width.indexOf( '%' ) ) {
				width = ( $( menu.settings.parent ).width() * parseInt( string_width ) ) / 100;
			}
			if ( 0 <= string_width.indexOf( 'px' ) ) {
				width = parseInt( string_width );
			}
			return width;
		};

		var detachStyles = function() {

			$( 'li' + parentMegaClass, menu ).each( function() {
				$( this ).children( 'a' ).removeAttr( 'styles' );
			} );

		};

		var appendStyles = function() {

			$( 'li' + parentMegaClass, menu ).each( function() {

				// all open children of open siblings
				var item        = $( this ),
					anchor      = $( this ).children( 'a' ),
					menuWidth   = null,
					type        = item.data( 'sub-type' ),
					windowWidth = Math.min( $( document ).width(), window.innerWidth ),
					position    = item.data( 'sub-position' ),
					styles      = {},
					parent      = menu.settings.parent,
					width,
					left,
					right,
					top;

				if ( 'fullwidth' != position ) {
					if ( windowWidth >= triggerFullscreen ) {
						menuWidth = item.data( 'width-fullscreen' );
					} else if ( windowWidth >= triggerDesktop && windowWidth < triggerFullscreen ) {
						menuWidth = item.data( 'width-desktop' );
					} else if ( windowWidth < triggerDesktop ) {
						menuWidth = item.data( 'width-tablet' );
					}
				}

				switch ( position ) {
					case 'fullwidth' :
						menuWidth   = '100%';
						styles.left = $( menu.settings.parent ).offset().left - menu.offset().left + parseInt( $( menu.settings.parent ).css( 'padding-left' ) ) + parseInt( $( menu.settings.parent ).css( 'border-left-width' ) );
						break;

					case 'center-parent' :
						styles.left = item.offset().left + item.outerWidth() / 2 - getMenuWidth( menuWidth ) / 2 - menu.offset().left;
						break;

					case 'left-parent' :
						left         = item.position().left;
						styles.left  = left;
						styles.right = 'auto';

						if ( '100%' == menuWidth && 0 < left ) {
							menuWidth    = 'auto';
							styles.right = 0;
						}

						break;

					case 'right-parent' :
						right        = menu.offset().left + parseInt( menu.css( 'border-left-width' ) ) + menu.width() - item.offset().left - item.width();
						styles.left  = 'auto';
						styles.right = right;

						if ( '100%' == menuWidth && 0 < right ) {
							menuWidth   = 'auto';
							styles.left = 0;
						}

						break;

					case 'vertical-parent' :
						styles.top = item.offset().top - menu.offset().top;

						break;
				}

				if ( 'vertical' == menu.settings.direction && 0 <= menuWidth.indexOf("%") ) {
					menuWidth = null;
				}

				if ( null !== menuWidth ) {
					styles.width = menuWidth;
				}


				anchor
					.siblings( subClass )
					.css( styles );

			} );

		};

		var hidePanel = function( anchor ) {

			if ( isMobile ) {
				return;
			}

			anchor
				.parent()
				.addClass( inTransitionClass )
				.removeClass( hoverClass )
				.triggerHandler( 'closePanel' );
		};

		var showPanel = function( anchor ) {

			if ( isMobile ) {
				return;
			}

			// all open children of open siblings
			anchor
				.parent()
				//.removeClass( inTransitionClass )
				.addClass( hoverClass )
				.triggerHandler( 'openPanel' );

			anchor
				.parent()
				.siblings()
				.removeClass( inTransitionClass );
		};

		var openOnClick = function() {

			if ( isMobile ) {

				$( document ).off( 'click touchstart', closeAllPanels );

				$( 'li' + parentMegaClass + ' > a', menu ).off( 'click', panelTriggerOnClick );

				return;
			}

			// hide menu when clicked away from
			$( document ).on( 'click touchstart', closeAllPanels );

			$( 'li' + parentMegaClass + ' > a', menu ).on( 'click', panelTriggerOnClick );
		};

		var closeAllPanels = function( event ) {

			if ( !$( event.target ).closest( '.tm-mega-menu li' ).length ) {

				hidePanel( $( '.' + hoverClass ).children( 'a' ) );
			}
		};

		var panelTriggerOnClick = function( event ) {

			var parent = $( event.target ).closest( 'li' );

			if ( parent.hasClass( hideMobileClass ) && menu.hasClass( mobileOnClass ) ) {

				return;
			}
			// check for second click
			if ( !parent.hasClass( hoverClass ) ) {

				event.preventDefault();

				hidePanel( $( '.' + hoverClass ).children( 'a' ) );

				showPanel( $( event.target ) );

			} else {

				hidePanel( $( event.target ) );
			}
		};

		var openOnHover = function() {

			if ( isMobile ) {

				//$( 'li' + parentMegaClass, menu ).off( 'hover', panelTriggerOnHover );
				return;
			}

			$( 'li' + parentMegaClass, menu ).on( 'hover', panelTriggerOnHover );
		};

		var panelTriggerOnHover = function ( event ) {

			var item   = $( event.currentTarget ),
				anchor = item.children( 'a' );

			// check if is nested item in mega sub menu
			if ( isInMegamenu( item ) ) {

				return;
			}
			if ( 'mouseenter' == event.type ) {

				showPanel( anchor );
			}
			if ( 'mouseleave' == event.type ) {

				hidePanel( anchor );
			}
		};

		var mobileToggle = function() {

			if ( menu.settings.mobileButton ) {

				var checkbox = $( '.tm-mega-menu-mobile-trigger-box' );

				$( menu.settings.mobileButton ).click( function() {

					checkbox.prop( 'checked', ! checkbox.prop( 'checked' ) );
				} );
			}
		};

		var init = function() {

			menu.removeClass( noJsClass );

			$( window ).on( 'debouncedresize', function( event ) {

				switchMobile();

				if ( isTouchDevice || 'click' === menu.settings.event ) {
					openOnClick();
				} else {
					openOnHover();
				}

			} ).trigger( 'debouncedresize' );

			mobileToggle();
		};

		init();
	};

	$( '.tm-mega-menu' ).each( function() {
		$( this ).megaMenu();
	} );
} )( jQuery );